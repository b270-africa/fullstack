// console.log("Hello World");

const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

txtFirstName.addEventListener('keyup', fullName);

txtFirstName.addEventListener('keyup', (event) => {
	
	console.log(event.target);

	console.log(event.target.value);
});

txtLastName.addEventListener('keyup', fullName);

txtLastName.addEventListener('keyup', (event) => {
	
	console.log(event.target);

	console.log(event.target.value);
});

function fullName() {
  const firstName = txtFirstName.value;
  const lastName = txtLastName.value;

  spanFullName.innerHTML = `${firstName} ${lastName}`;
}


